# PulseAudio Study

* this projects collects some source code examples using PulseAudio 
* it is not production code 
* it is for demonstrative purposes


| Executable               | Explanation                                                                                             | 
|--------------------------|---------------------------------------------------------------------------------------------------------|
| src/**block-play**       | simplest form of audio playback: it blocks main thread while feeding the audio sink                     |
| src/**async-play-sine**  | async playback of a sine wave - loops forever                                                           |
| src/**async-play-file** | reads & plays audio file in an async manner; terminates upon completion |                                                |
| src/**async-play-volume** | modifies audio volume per channel                                                                       |




# Getting Started
## Dependencies
  * `cmake` -> as bulid tool
  * `extra-cmake-modules` -> to find PulseAudio

``` 
sudo apt-get install cmake extra-cmake-modules
```

## Build and Run

``` 
$ mkdir build 
$ cmake .. 
$ make
$ ./blocking-playback
```

## CMake notes
  * there is no CMake configuration provided by PulseAudio developers - or is there?
  * `extra-cmake-modules` (aka. `ECM`) provides scripts to find PulseAudio on the system 
  * [CMakeLists.txt](CMakeLists.txt) adds ECM package in order to find PulseAudio (using an `include`) 

```CMake
find_package(ECM REQUIRED NO_MODULE 
set(CMAKE_MODULE_PATH ${ECM_MODULE_PATH})
include(FindPulseAudio)
```
 
  * note that `PulseAudio::PulseAudio` does **not** include `libpulse-simple.so` 
     * causing linker error for `block-play` (and any simple/blocking PulseAudio code)
     * `pulse-simple` must be added as an extra library dependency with `target_link_libraries`
```CMake 
target_link_libraries(block-play
    PUBLIC PulseAudio::PulseAudio
    pulse-simple                   # missing from 'PulseAudio::PulseAudio'
)
```


# References
  * [PulseAudio  GitLab repo](https://gitlab.freedesktop.org/pulseaudio/pulseaudio)
  * [Official PulseAudio  dev docs](https://freedesktop.org/software/pulseaudio/doxygen/)
  * [PulseAudio under the hood](https://gavv.net/articles/pulseaudio-under-the-hood/): missing documentation for PulseAudio
  * [VLC PulseAudio module](https://code.videolan.org/videolan/vlc/-/blob/master/modules/audio_output/)
  * Sample code of [AsyncPlayback](https://www.freedesktop.org/wiki/Software/PulseAudio/Documentation/Developer/Clients/Samples/AsyncPlayback/) 
    * a little bit misleading
    * focuses on latency





# Legal notes
* audio files in the `data` dir are downloaded from [The Open Speech Repository](https://www.voiptroubleshooter.com/open_speech/american.html)  (October 2023)

