#include <stdio.h>
#include <string.h>
#include <pulse/pulseaudio.h>
#include <pulse/def.h>
#include <math.h>

// cf. https://www.freedesktop.org/wiki/Software/PulseAudio/Documentation/Developer/Clients/Samples/AsyncPlayback/
// cf. http://www.bic.mni.mcgill.ca/software/MDP/HTML/VOLUME_IO_prog_ref.html/volume-source-code-example.html

typedef struct wave_data {
  int16_t* buff;
  int buff_size;  // buff_size in samples
  int buff_off;   // offset in samples
} wave_data_t;

const double PI = 3.14159265358979323846;

/**
* Generates audio buff form of specified duration.
* @param dur_in_sec
* @param hz
* @return non owning pointer
*/
wave_data_t* gen_audio_data(const int dur_in_sec, const double hz){
  wave_data_t* data = malloc(sizeof(wave_data_t));
  data->buff_size = (int)(dur_in_sec * hz);  // number of samples
  data->buff = malloc(data->buff_size * sizeof(int16_t));
  for (int i=0; i < data->buff_size; i++) {
    double amp = sin(2 * PI * ((double)i/hz) * 240 );
    data->buff[i] = (int16_t) (amp * INT16_MAX );
  }
  data->buff_off = 0;
  return data;
}

typedef struct pa_loop_ctx {
  pa_mainloop*         pa_loop;
  pa_mainloop_api*     pa_api;
  pa_context*          pa_ctx;
} pa_ctx_t;


/**
 * Allocates and initializes PulseAudio objects.
 * @return a struct with initialized objects.
 */
pa_ctx_t* pa_loop_ctx_new(){
  pa_ctx_t* loop_ctx = (pa_ctx_t*) malloc(sizeof(pa_ctx_t));
  // Create PulseAudio mainloop API and connection to the default server
  loop_ctx->pa_loop = pa_mainloop_new();
  loop_ctx->pa_api  = pa_mainloop_get_api(loop_ctx->pa_loop);
  loop_ctx->pa_ctx  = pa_context_new(loop_ctx->pa_api, "PA async playback demo");
  return loop_ctx;
}

/**
 * Disconnect from PulseAudio server and free allocatedresources.
 * @param loop
 */
void pa_loop_ctx_free(pa_ctx_t* loop) {
  pa_context_disconnect(loop->pa_ctx);
  pa_context_unref(loop->pa_ctx);
  pa_mainloop_free(loop->pa_loop);
}

/**
 * This callback gets called when the context changes state.
 * We really only care about when it's ready or if it has failed
 * @param c
 * @param userdata
 */
void pa_state_callback(pa_context *ctx, void *userdata) {
  int* pa_ready = (int*)userdata;
  pa_context_state_t state = pa_context_get_state(ctx);
  switch  (state) {
    // These are just here for reference
    case PA_CONTEXT_UNCONNECTED:
      fprintf(stdout, "PA unconnected\n");
      break;
    case PA_CONTEXT_CONNECTING:
      fprintf(stdout, "PA unconnecting\n");
      break;
    case PA_CONTEXT_AUTHORIZING:
      fprintf(stdout, "PA authorizing\n");
      break;
    case PA_CONTEXT_SETTING_NAME:
      fprintf(stdout, "PA setting context name\n");
      break;
    case PA_CONTEXT_FAILED:
      fprintf(stdout, "PA context failed\n");
      break;
    case PA_CONTEXT_TERMINATED:
      fprintf(stdout, "PA context terminated\n");
      *pa_ready = 2;
      break;
    case PA_CONTEXT_READY:
      fprintf(stdout, "PA context ready\n");
      *pa_ready = 1;
      break;
    default:
      fprintf(stdout, "PA uknown state: %d\n", state);
      break;
  }
}

/**
 * Just a placeholder to avoid copying sample in 'pa_stream_write'
 */
void null_fun(void*){};


void stream_request_cb(pa_stream *stream, size_t length, void *userdata) {
  wave_data_t* data = (wave_data_t*) userdata;
  int nbyte_left, nbyte_to_write;

  while(length > 0){
    nbyte_left = (data->buff_size - data->buff_off) * 2;
    nbyte_to_write = (int)fmin(length,nbyte_left);
    fprintf(stdout, "%6d data-offset; %6d avail-length; %6d left-in-buff, %6d to-write\r",
            2*data->buff_off, (int)length, nbyte_left, nbyte_to_write);
    int err_no = pa_stream_write(
        stream, &(data->buff[data->buff_off]),
        nbyte_to_write,
        null_fun, 0LL, PA_SEEK_RELATIVE);
    if(err_no != 0){
      fprintf(stderr, "failed to write audio stream. Error %s [%d]\n", pa_strerror(err_no), err_no );
      return;
    }
    data->buff_off += nbyte_to_write / 2;
    length -= nbyte_to_write;
    if (data->buff_off == data->buff_size){
      data->buff_off = 0;
    }
  }
}


int main() {
  int err_no;
  int retval;
  int pa_ready = 0;
  pa_sample_spec af = {PA_SAMPLE_S16LE, 8000, 1};

  wave_data_t* data = gen_audio_data(1, af.rate);

  pa_ctx_t* ctx = pa_loop_ctx_new();

  pa_context_set_state_callback(ctx->pa_ctx, pa_state_callback, &pa_ready);

  err_no = pa_context_connect(
      ctx->pa_ctx,
      NULL, // NULL: default server
      0,     //
      NULL   // callback when new daemon is spawned
      );
  if (err_no != 0){
    fprintf(stderr, "Could not create PulseAudio context. Error %s [%d]\n", pa_strerror(err_no), err_no );
    pa_loop_ctx_free(ctx);
    return 1;
  }

  // Iterate mainloop till PA gets ready
  // 'pa_ready' is set in callback fun 'pa_state_callback'
  while (pa_ready == 0) {
    int n_sources = pa_mainloop_iterate(ctx->pa_loop, 1, &retval);
    if(n_sources < 1){
      fprintf(stdout, "pa_mainloop_iterate could not create any sources: err=%d\n", n_sources);
      return 3;
    }
  }
  if (pa_ready == 2) {
    return -1;
  }

  pa_stream *playstream = pa_stream_new(ctx->pa_ctx, "Playback", &af, NULL);
  if (!playstream) {
    printf("pa_stream_new failed\n");
  }

  pa_stream_set_write_callback(playstream, stream_request_cb, data);

  err_no = pa_stream_connect_playback(playstream, NULL, NULL,
                                 PA_STREAM_INTERPOLATE_TIMING
                                     |PA_STREAM_ADJUST_LATENCY
                                     |PA_STREAM_AUTO_TIMING_UPDATE, NULL, NULL);
  if (err_no != 0) {
    fprintf(stderr, "Could not connect playback stream. Error %s [%d]\n", pa_strerror(err_no), err_no);
    return 2;
  }

  { // status print
    pa_context_state_t state = pa_context_get_state(ctx->pa_ctx);
    retval = PA_CONTEXT_IS_GOOD(state);
    if(retval != 1){
      fprintf(stdout, "Error state before audio loop: %d  OK=%d\n", state, retval);
      return 3;
    }
    fprintf(stdout, "State before audio loop: %d  OK=%d\n", state, retval);
  }
  // Run the mainloop until pa_mainloop_quit() is called
  // (this example never calls it, so the mainloop runs forever).
  err_no = pa_mainloop_run(ctx->pa_loop, &retval);
  if (err_no != 0) {
    fprintf(stderr, "Could not connect playback stream. Error %s [%d]\n", pa_strerror(err_no), err_no);
    return 4;
  }

  return 0;
}
