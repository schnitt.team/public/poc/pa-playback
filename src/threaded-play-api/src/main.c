#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "threaded-play-api.h"


const char * SAMPLE_AUDIO_FILE = DEV_SAMPLE_WAV;  // as defined in 'CMakeLists.txt'


int16_t* read_audio(size_t nsample){
    FILE* fh = fopen(SAMPLE_AUDIO_FILE, "rb");
    fseek(fh, 44, SEEK_SET); // skip 44 byte WAVE header

    int16_t* data = malloc(nsample * sizeof(int16_t));

    int off = 0;
    while(off*2 < nsample){
        off += fread(&(data[off]), 1,  (nsample*2 - off), fh);
    }
    fclose(fh);
    return data;
}


void test_play() {
    int nsample = 8000 * 5; // 5 sec
    int16_t *data = read_audio(nsample);

    BufferPlayer *player = buffpla_new();
    player->is_loop = 0;
    buffpla_set_buff(player, data, nsample);
}


void test_cli(){
    BufferPlayer *player = buffpla_new();
    player->is_loop = 0;

    int nsample = 8000 * 30; // 5 sec
    int16_t *data = read_audio(nsample);
    buffpla_set_buff(player, data, nsample);


    if(BUFFPLA_OK != buffpla_init_ctx(player)){
        return;
    }
    if(BUFFPLA_OK != buffpla_connect_stream(player)){
        return;
    }

    char cmd_buff[100];
    fprintf(stdout, "buffpla> ");
    char * word;
    char * start_s;
    char * end_s;
    while(fgets(cmd_buff, 100, stdin)) {
        cmd_buff[strlen(cmd_buff) - 1] = '\0';

        word = strtok(cmd_buff, " ");

        if (!word) break;
        if(strcmp(word, "time") == 0){
            pa_usec_t u_sec;
            if(BUFFPLA_OK == buffpla_get_ellapsed(player, &u_sec)){
                float sec = u_sec / 2 / 8000.0F;
                fprintf(stdout, "->Time: %8.3f sec\n", sec);
            }else{
                fprintf(stdout, "->Time: n/a\n");
            }
        }else if(strcmp(word, "play") == 0 || strcmp(word, "play") == 0) {
            fprintf(stdout, "->Play\n");
            buffpla_start(player);
        }else if(strcmp(word, "pau") == 0 || strcmp(word, "pause") == 0) {
            buffpla_toggle(player);
        }else if(strcmp(word, "loop") == 0){
            if(player->is_loop)
                fprintf(stdout, "->loop OFF\n");
            else
                fprintf(stdout, "->loop ON\n");
            buffpla_set_loop(player, !player->is_loop);
        }else if(strcmp(word, "range") == 0){
            start_s = strtok(NULL, " ");
            end_s = strtok(NULL, " ");
            float start = atof(start_s);
            float end = atof(end_s);
            if(!start || !end){
                fprintf(stderr, "Syntax error! Usage 'range <START_IN_SEC> <END_IN_SEC>'");
                continue;
            }else{
                set_range(player, (int)(start * 8000), (int)(end * 8000));
            }
        }else{
            fprintf(stdout, "What?\n");
        }
        fprintf(stdout, "buffpla> ");
    }
}


int main(){
//    test_play();
    test_cli();


}