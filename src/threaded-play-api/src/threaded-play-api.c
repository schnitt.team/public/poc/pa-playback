#include "threaded-play-api.h"
#include <stdio.h>
#include <math.h>
#include <memory.h>
//#include <pulsecore/log.h>

#define CLEAR_LINE "\x1B[K"

void buffla_on_write_req(pa_stream *stream, size_t req_len, void *userdata);

BufferPlayer* buffpla_new(){
    BufferPlayer* ptr = malloc(sizeof(BufferPlayer));
    ptr->pa_af = malloc(sizeof(pa_sample_spec));
    ptr->pa_af->channels = 1;
    ptr->pa_af->format = PA_SAMPLE_S16LE;
    ptr->pa_af->rate = 8000;

    // rest is NULL initialized
    ptr->buff    = NULL;
    ptr->buff_own  = 0;
    ptr->buff_size = 0;
    ptr->buff_off  = 0;
    ptr->is_loop = 1;

    ptr->pa_ctx  = NULL;
    ptr->pa_api  = NULL;
    ptr->pa_loop = NULL;
    ptr->stream  = NULL;

    return ptr;
}


void buffpla_on_stream_state_change(pa_stream *stream, void *userdata) {
    pa_stream_state_t state = pa_stream_get_state(stream);
    switch (state) {
        case PA_STREAM_UNCONNECTED: /**< The stream is not yet connected to any sink or source */
            fprintf(stderr, "[STR_STATUS]  unconnected\n");
            break;
        case PA_STREAM_CREATING:    /**< The stream is being created */
            fprintf(stderr, "[STR_STATUS]  creating\n");
            break;
        case PA_STREAM_READY:       /**< The stream is established, you may pass audio data to it now */
            fprintf(stderr, "[STR_STATUS]  ready\n");
            break;
        case PA_STREAM_FAILED:      /**< An error occurred that made the stream invalid */
            fprintf(stderr, "[STR_STATUS]  failed\n");
            break;
        case PA_STREAM_TERMINATED:  /**< The stream has been terminated cleanly */
            fprintf(stderr, "[STR_STATUS]  terminated\n");
            break;
    }
}

void buffpla_on_ctx_state_change(pa_context *ctx, void *userdata) {
//    int* is_ready = (int*)userdata;
    BufferPlayer * player = (BufferPlayer *)userdata;
    pa_context_state_t state = pa_context_get_state(ctx);
    switch  (state) {
        // These are just here for reference
        case PA_CONTEXT_UNCONNECTED:
            fprintf(stderr, "[CTX_STATUS]  unconnected\n");
            break;
        case PA_CONTEXT_CONNECTING:
            fprintf(stderr, "[CTX_STATUS]  connecting\n");
            break;
        case PA_CONTEXT_AUTHORIZING:
            fprintf(stderr, "[CTX_STATUS]  authorizing\n");
            break;
        case PA_CONTEXT_SETTING_NAME:
            fprintf(stderr, "[CTX_STATUS]  setting context name\n");
            break;
        case PA_CONTEXT_FAILED:
            fprintf(stderr, "[CTX_STATUS]  context failed\n");
            break;
        case PA_CONTEXT_TERMINATED:
            fprintf(stderr, "[CTX_STATUS]  context terminated\n");
//            *is_ready = 2;
            break;
        case PA_CONTEXT_READY:
            fprintf(stderr, "[CTX_STATUS]  context ready\n");
//            *is_ready = 1;
            pa_threaded_mainloop_signal(player->pa_loop, 0);
            break;
        default:
            fprintf(stderr, "[CTX_STATUS]  uknown state: %d\n", state);
            break;
    }

}

void null_cb(void* userdata){};

static void stream_wait_cb(pa_stream *s, int success, void *userdata){
    (void) s; (void) success;
    pa_threaded_mainloop_signal(userdata, 0);
}

void on_cork_complete(pa_stream* stream, int success, void *userdata) {
    fprintf(stdout, "on_cork_complete: success=%d\n", success);
    if(userdata) {
        BufferPlayer *player = (BufferPlayer *) userdata;
//        pa_operation_unref(player->pa_op_cork);
//        player->pa_op_cork = NULL;
        fprintf(stderr, "Cork DONE (2)");
    }
}


void on_drain_op_done(pa_operation* op, void *userdata){
    pa_operation_state_t state = pa_operation_get_state(op);
    if (PA_OPERATION_DONE == state) {
//        fprintf(stderr, "Drain DONE");
        BufferPlayer *player = (BufferPlayer *) userdata;
        pa_operation_unref(player->pa_op_drain);
        player->pa_op_drain = NULL;
    }

}

void on_drain_stream_complete(pa_stream* stream, int success, void *userdata){
    fprintf(stdout, "on_drain_stream_complete: success=%d\n", success);
    BufferPlayer *player = (BufferPlayer *) userdata;
    pa_operation_state_t state_drain = pa_operation_get_state(player->pa_op_drain);
    if (PA_OPERATION_DONE == state_drain) {
        fprintf(stderr, "Drain DONE");
    }
}

static void stream_suspended_callback(pa_stream *stream, void *userdata) {
    if (pa_stream_is_suspended(stream))
        fprintf(stdout, "Stream device suspended");
    else
        fprintf(stdout, "Stream device resumed");
}

void buffla_on_stream_event(pa_stream *stream, const char *name, pa_proplist *pl, void *userdata){
    fprintf(stdout, "[EVENT] %s", name);
}


buffpla_state_t write_playback_buff(BufferPlayer* player, size_t req_len, size_t* written){
    size_t nbyte_to_write = (int)fmin(req_len, (player->buff_end - player->buff_off)*2);
    int err_no = pa_stream_write(player->stream, &(player->buff[player->buff_off]), nbyte_to_write,null_cb, 0LL, PA_SEEK_RELATIVE);
    if(err_no != 0){
        fprintf(stderr, "failed to write audio stream. Error %s [%d]\n", pa_strerror(err_no), err_no );
        return ERR_WRITE_PA_STREAM;
    }
    *written = nbyte_to_write;
    player->buff_off += nbyte_to_write/2; // nbyte_to_write/2;
    return BUFFPLA_OK;
}


void buffla_on_write_req(pa_stream *stream, size_t req_len, void *userdata){
    BufferPlayer* player = (BufferPlayer*) userdata;
    pa_usec_t ellapsed;
    if(BUFFPLA_OK == buffpla_get_ellapsed(player, &ellapsed)){
        float sec = ellapsed / 2.0F / 8000.0F;
        fprintf(stdout, "ellapsed %8.3f sec\n", sec);
    }

    if(player->pa_op_drain){
        fprintf(stdout, "buffla_on_write_req: %zu (SKIP)\n", req_len);
        return;
    }else{
//        fprintf(stdout, "buffla_on_write_req: %zu\n", req_len);
    }

    size_t nbyte_to_write = req_len;
    while(req_len > 0) {  // fill up that buffer
        nbyte_to_write = (int) fmin(req_len, (player->buff_end - player->buff_off) * 2);
        buffpla_state_t write_state = write_playback_buff(player, nbyte_to_write, &nbyte_to_write);
        if(BUFFPLA_OK != write_state){
            fprintf(stderr, "Failed to write buffer: err_no=%d", write_state);
            break;
        }
        req_len -= nbyte_to_write;

        if (player->buff_off == player->buff_end) {
            player->buff_off = player->buff_start;
            if (!player->is_loop) {
                player->pa_op_drain = pa_stream_drain(player->stream, NULL, player);
                pa_operation_set_state_callback(player->pa_op_drain, on_drain_op_done, player);
//            }else{
//                fprintf(stdout, "loop!\n");
            }
        }
    }
}


buffpla_state_t buffpla_init_ctx(BufferPlayer* player){
    // Get a mainloop and its context
    player->pa_loop = pa_threaded_mainloop_new();
    if (player->pa_loop == NULL){
        fprintf(stderr, "Could not create a pa_threaded_mainloop.");
        return ERR_CREATE_PA_MAINLOOP;
    }

    player->pa_api = pa_threaded_mainloop_get_api(player->pa_loop);
    player->pa_ctx = pa_context_new(player->pa_api, "Buffer Player");
    if (player->pa_ctx == NULL){
        return ERR_CREATE_PA_CONTEXT;
    }

    // Set a callback so we can wait for the context to be ready
    pa_context_set_state_callback(player->pa_ctx, &buffpla_on_ctx_state_change, player);

    // Lock the mainloop so that it does not run and crash before the context is ready
    pa_threaded_mainloop_lock(player->pa_loop);

    if (pa_threaded_mainloop_start(player->pa_loop) != 0){
        return ERR_START_MAINLOOP;
    }

    if (pa_context_connect(player->pa_ctx, NULL, 0, NULL) != 0){
        fprintf(stderr, "failed to connect to the pulseaudio context -- the error message is \"%s\".",
                 pa_strerror(pa_context_errno(player->pa_ctx)));
        return ERR_CONNECT_PA_CONTEXT;
    }

    // Wait for the context to be ready
    pa_context_state_t ctx_state;
    while ((ctx_state = pa_context_get_state(player->pa_ctx) != PA_CONTEXT_READY)) {
        if (!PA_CONTEXT_IS_GOOD(ctx_state)){
            fprintf(stdout, "pa context is not good -- the error message \"%s\".\n",
                    pa_strerror(pa_context_errno(player->pa_ctx)));
            return ERR_CREATE_PA_CONTEXT;
        }
        pa_threaded_mainloop_wait(player->pa_loop);
    }

    pa_threaded_mainloop_unlock(player->pa_loop);
    return BUFFPLA_OK;
}

buffpla_state_t buffpla_connect_stream(BufferPlayer* player){
    pa_threaded_mainloop_lock(player->pa_loop);
    // Create a playback stream

//    pa_channel_map map;
//    pa_channel_map_init_stereo(&map);

    player->stream = pa_stream_new(player->pa_ctx, "BufferPlayer", player->pa_af, NULL);
    pa_stream_set_state_callback(player->stream, buffpla_on_stream_state_change, player);
    pa_stream_set_write_callback(player->stream, buffla_on_write_req, player);
    pa_stream_set_event_callback(player->stream, buffla_on_stream_event, player);

//    pa_stream_flags_t flags = PA_STREAM_INTERPOLATE_TIMING
//                              | PA_STREAM_AUTO_TIMING_UPDATE
//                              | PA_STREAM_START_CORKED
//                              | PA_STREAM_NOT_MONOTONIC
//                              | PA_STREAM_AUTO_TIMING_UPDATE
//                              | PA_STREAM_FIX_RATE;

    pa_stream_flags_t flags = 0
            | PA_STREAM_INTERPOLATE_TIMING
            | PA_STREAM_AUTO_TIMING_UPDATE
            | PA_STREAM_START_CORKED
//            | PA_STREAM_NOT_MONOTONIC
            ;


    int err_no = pa_stream_connect_playback(player->stream, NULL, NULL, //&bufattr,
                                            flags, NULL, NULL);
    if (err_no != 0) {
        return ERR_CONNECT_PA_STREAM;
//        fprintf(stderr, "Could not connect playback stream. Error %s [%d]\n", pa_strerror(err_no), err_no);
//        return 4;
    }

    pa_threaded_mainloop_unlock(player->pa_loop);
}


void buffpla_set_buff(BufferPlayer* player, int16_t* samples, size_t size){
    if(player->buff_own){ // free if owning the pointer
        free(player->buff);
    }

    player->buff = samples;

    player->buff_own = 0;  // true
    player->buff_end = player->buff_size = size;
    player->buff_start = player->buff_off = 0;
}


buffpla_state_t set_range(BufferPlayer* player, int off_start, int off_end){
    pa_operation *op;
    pa_threaded_mainloop_lock(player->pa_loop);
        op = pa_stream_cork(player->stream, 1, NULL, NULL);
    pa_threaded_mainloop_unlock(player->pa_loop);
    if(op){
        while (PA_OPERATION_DONE != pa_operation_get_state(op)){
            pa_threaded_mainloop_wait(player->pa_loop);
        }
        pa_operation_unref(op);
        // flush remaning

    }
    pa_stream_flush(player->stream, NULL, player);

    pa_threaded_mainloop_lock(player->pa_loop);
    if (off_start < 0) return ERR_OUT_OF_BOUND_OFFSET;
    if (off_end > player->buff_size) return ERR_OUT_OF_BOUND_OFFSET;
    if (off_start == off_end) return ERR_INVALID_OFFSET_RANGE;
    if (off_start > off_end) return ERR_INVALID_OFFSET_RANGE;
    player->buff_start = player->buff_off = off_start;
    player->buff_end = off_end;
    pa_threaded_mainloop_unlock(player->pa_loop);
    return BUFFPLA_OK;
}


buffpla_state_t buffpla_toggle(BufferPlayer* player){
    pa_operation *op;
    pa_threaded_mainloop_lock(player->pa_loop);
    if(pa_stream_is_corked(player->stream)){
        op = pa_stream_cork(player->stream, 0, NULL, NULL);
        if(op) op = NULL;
        op = pa_stream_trigger(player->stream, NULL, NULL);
    }else{
        op = pa_stream_cork(player->stream, 1, NULL, NULL);
    }
    pa_threaded_mainloop_unlock(player->pa_loop);

    if (op != NULL){
        while (PA_OPERATION_RUNNING == pa_operation_get_state(op))
            pa_threaded_mainloop_wait(player->pa_loop);
        pa_operation_unref(op);
    }
}

buffpla_state_t buffpla_set_loop(BufferPlayer* player, int b){
    pa_threaded_mainloop_lock(player->pa_loop);
    player->is_loop = b;
    pa_threaded_mainloop_unlock(player->pa_loop);
}


buffpla_state_t buffpla_get_ellapsed(BufferPlayer* player, pa_usec_t* ellapsed) {
//    pa_operation *op = pa_stream_update_timing_info(player->stream, NULL, NULL);
//    if (op) pa_operation_unref(op);

//    pa_usec_t ellapsed;
//    if (pa_stream_get_time(player->stream, &ellapsed) >= 0) {
//        *samples = ellapsed - player->read_start;
//        return BUFFPLA_OK;
//    } else {
//        return ERR_TIMING_CORRUPT;
//    }
//}

    const pa_timing_info *ts;
    ts = pa_stream_get_timing_info(player->stream);
    if (ts) {
        if (ts->read_index_corrupt) return ERR_TIMING_CORRUPT;
//        fprintf(stdout, "%ld %ld %zu\n", ts->read_index, ts->write_index, player->buff_off);
        *ellapsed = ts->read_index - player->read_start;
//        *samples = ts->read_index - player->read_start;
        return BUFFPLA_OK;
    }
    return ERR_TIMING_NOT_AVAIL;
}


buffpla_state_t buffpla_start(BufferPlayer* player){
    pa_operation *op;

    pa_threaded_mainloop_lock(player->pa_loop);

    if(player->pa_op_drain){
//        pa_operation_state_t  state = pa_operation_get_state(player->pa_op_drain);
//        while (PA_OPERATION_DONE != state && PA_OPERATION_CANCELLED != state)
        fprintf(stdout, "pa-op-drain wait\n");
        while(!player->pa_op_drain) // NOTE: callback 'pa_op_drain' must have a notify callback that nulls 'pa_op_drain'
            pa_threaded_mainloop_wait(player->pa_loop);
    }

    if(!pa_stream_is_corked(player->stream)){
        op = pa_stream_cork(player->stream, 1, NULL, NULL);
        if (op != NULL) pa_operation_unref(op);
    }

    // write some, if needed
//    pa_threaded_mainloop_lock(player->pa_loop);
    {
        size_t size_req = pa_stream_writable_size(player->stream);
        if (size_req > 0){
            size_t written;
            write_playback_buff(player, size_req, &written);
//            fprintf(stdout, "pre-port %zu\n", written);
        }
    }

    op = pa_stream_update_timing_info(player->stream, NULL, NULL);
    if (op != NULL) pa_operation_unref(op);

//    pa_stream_update_timing_info(player->stream, NULL, NULL);
//    pa_usec_t ellapsed;
//    if(pa_stream_get_time(player->stream, &ellapsed) >= 0){
//        player->read_start = ellapsed;
//    }else{
//        player->read_start = -1;
//    }

    const pa_timing_info* ts = pa_stream_get_timing_info(player->stream);
    if(ts) {
        player->read_start = ts->read_index;
//        fprintf(stdout, "%ld %ld %zu\n", ts->read_index, ts->write_index, player->buff_off);
    }else{
        player->read_start = -1;
    }

    // uncork
    op = pa_stream_cork(player->stream, 0, NULL, NULL);
    if (op != NULL) pa_operation_unref(op);

    op = pa_stream_trigger(player->stream, NULL, NULL);
    if (op) pa_operation_unref(op);

//    const pa_timing_info* ts = pa_stream_get_timing_info(player->stream);
//    if(ts) {
//        player->read_start = ts->read_index;
//        fprintf(stdout, "%ld %ld %zu\n", ts->read_index, ts->write_index, player->buff_off);
//    }

//    if (op != NULL){
//        while (PA_OPERATION_RUNNING   == pa_operation_get_state(op)
//            || PA_OPERATION_CANCELLED == pa_operation_get_state(op)){
//            pa_threaded_mainloop_wait(player->pa_loop);
//        }
//        pa_operation_unref(op);
//    }


    pa_threaded_mainloop_unlock(player->pa_loop);

}
