#ifndef BUFFER_PLAYER_THREADED_PLAY_API_H_
#define BUFFER_PLAYER_THREADED_PLAY_API_H_

#include <stddef.h>
#include <stdint.h>
#include <pulse/pulseaudio.h>
#include <pulse/def.h>

typedef enum BUFFPLA_STATUS {
    BUFFPLA_OK                 = 0,
    ERR_AUDIO_NOT_INITIALIZED  = 111,
    ERR_OUT_OF_BOUND_OFFSET    = 410,
    ERR_INVALID_OFFSET_RANGE   = 420,
    ERR_CREATE_PA_MAINLOOP     = 510,

    ERR_CREATE_PA_CONTEXT      = 520,
    ERR_CONNECT_PA_CONTEXT     = 521,

    ERR_CREATE_PA_STREAM       = 530,
    ERR_CONNECT_PA_STREAM      = 531,
    ERR_WRITE_PA_STREAM        = 532,

    ERR_START_MAINLOOP         = 512,

    ERR_TIMING_NOT_AVAIL       = 601,
    ERR_TIMING_CORRUPT         = 602

} buffpla_state_t;


/**
 *
 *  |--------|--------------|--------|
 *         start           end     size
 *
 */
typedef struct BufferPlayer{
  int16_t* buff;    // buffer of samples
    size_t  buff_start;   // offset start in samples
    size_t  buff_end;     // offset end in samples
    size_t  buff_size;    // buffer size in samples
    size_t  buff_off;     // current offset in buffer in samples. start <= off <= end
    size_t  read_start;
  int buff_own;     // flag if owning the buff array -> to be freed or not
  int is_loop;      // flog for audio looping
  pa_operation* pa_op_drain;
  //
  pa_stream*             stream;
  pa_context*            pa_ctx;
  pa_threaded_mainloop* pa_loop;
  pa_mainloop_api*       pa_api;
  pa_sample_spec*         pa_af;
  pa_cvolume             pa_vol;
} BufferPlayer;




BufferPlayer* buffpla_new();

buffpla_state_t buffpla_init_ctx(BufferPlayer* player);

buffpla_state_t buffpla_connect_stream(BufferPlayer* player);


/**
 *
 * @param player player context
 * @param samples  data array
 * @param size  size in samples
 */
void buffpla_set_buff(BufferPlayer* player, int16_t* samples, size_t size);


///////////////////////////////////////////////////////////////
///                      PLAYBACK API                       ///
///////////////////////////////////////////////////////////////

/**
 * Sets range within the buffer what to play.
 * Stops ongoing playback and drains the playback buffer.
 * Constraints on the values: 0 <= start < end <= buffer size.
 *
 * @param player
 * @param start  start in samples. Must be between
 * @param end    end of the range in samples. If end <= 0, end equals to buffer size.
 * @return
 */
buffpla_state_t set_range(BufferPlayer* player, int start, int end);

buffpla_state_t buffpla_start(BufferPlayer* player);

buffpla_state_t buffpla_toggle(BufferPlayer* player);

buffpla_state_t buffpla_set_loop(BufferPlayer* player, int b);

buffpla_state_t buffpla_stop(BufferPlayer* player);

buffpla_state_t reset_range(BufferPlayer* player);

buffpla_state_t buffpla_get_ellapsed(BufferPlayer* player, size_t* samples);


///////////////////////////////////////////////////////////////
///                      Internal API                       ///
///////////////////////////////////////////////////////////////

void buffpla_on_ctx_state_change(pa_context *ctx, void *userdata);

void buffpla_on_stream_state_change(pa_stream *stream, void *userdata);


#endif //  BUFFER_PLAYER_THREADED_PLAY_API_H_
