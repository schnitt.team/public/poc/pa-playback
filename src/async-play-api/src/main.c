#include <stdint.h>
#include <stdio.h>

#include "async-play-api.h"


const char * SAMPLE_AUDIO_FILE = TEST_SAMPLE_WAV;  // as defined in 'CMakeLists.txt'


int16_t* read_audio(size_t nsample){
    FILE* fh = fopen(SAMPLE_AUDIO_FILE, "rb");
    fseek(fh, 44, SEEK_SET); // skip 44 byte WAVE header

    int16_t* data = malloc(nsample * sizeof(int16_t));

    int off = 0;
    while(off*2 < nsample){
        off += fread(&(data[off]), 1, 2 * (nsample*2 - off), fh);
    }
    fclose(fh);
    return data;
}


void test_new(){
    BufferPlayer * player = buffpla_new();
    buffpla_free(player);
}

void test_buff_init(){
    BufferPlayer * player = buffpla_new();
    buffpla_init_buff(player, 32);
    buffpla_free(player);
}

void test_buff_set(){
    BufferPlayer * player = buffpla_new();
    buffpla_init_buff(player, 32);

    int len = 12;
    int16_t* data = malloc( len * sizeof(int16_t));
    for(int i = 0; i < len; i++) data[i] = -i;
    buffpla_set_buff(player, data, len);

    buffpla_free(player);
    free(data);
}


void test_init(){
    BufferPlayer * player = buffpla_new();
    int status = buffpla_init(player);
    fprintf(stdout, "Init status=%d", status);

    buffpla_free(player);
}


void test_play(){
    int nsample = 8000 * 5; // 5 sec
    int16_t* data = read_audio(nsample);

    BufferPlayer * player = buffpla_new();
    player->is_loop = 0;
    buffpla_set_buff(player, data, nsample);


    if(buffpla_init(player) == 0){
      buffpla_blocking_play(player, 8000, 25000, 0);
    };

    buffpla_free(player);
    free(data);
}

int main(){
//    test_new();
//    test_buff_init();
//    test_buff_set();
//    test_init();
    test_play();

    return 0;
}