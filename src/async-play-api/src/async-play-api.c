#include "async-play-api.h"
#include <stdio.h>
#include <math.h>



void buffpla_state_listener(pa_context *ctx, void *userdata) {
    int* is_ready = (int*)userdata;
    pa_context_state_t state = pa_context_get_state(ctx);
    switch  (state) {
        // These are just here for reference
        case PA_CONTEXT_UNCONNECTED:
            fprintf(stderr, "[PA_STATUS]  unconnected\n");
            break;
        case PA_CONTEXT_CONNECTING:
            fprintf(stderr, "[PA_STATUS]  connecting\n");
            break;
        case PA_CONTEXT_AUTHORIZING:
            fprintf(stderr, "[PA_STATUS]  authorizing\n");
            break;
        case PA_CONTEXT_SETTING_NAME:
            fprintf(stderr, "[PA_STATUS]  setting context name\n");
            break;
        case PA_CONTEXT_FAILED:
            fprintf(stderr, "[PA_STATUS]  context failed\n");
            break;
        case PA_CONTEXT_TERMINATED:
            fprintf(stderr, "[PA_STATUS]  context terminated\n");
            *is_ready = 2;
            break;
        case PA_CONTEXT_READY:
            fprintf(stderr, "[PA_STATUS]  context ready\n");
            *is_ready = 1;
            break;
        default:
            fprintf(stderr, "[PA_STATUS]  uknown state: %d\n", state);
            break;
    }
}

BufferPlayer* buffpla_new(){
    BufferPlayer* ptr = malloc(sizeof(BufferPlayer));
    ptr->pa_af = malloc(sizeof(pa_sample_spec));
    ptr->pa_af->channels = 1;
    ptr->pa_af->format = PA_SAMPLE_S16LE;
    ptr->pa_af->rate = 8000;

    // rest is NULL initialized
    ptr->buff    = NULL;
    ptr->buff_own  = 0;
    ptr->buff_size = 0;
    ptr->buff_off  = 0;
    ptr->is_loop = 1;

    ptr->pa_ctx  = NULL;
    ptr->pa_api  = NULL;
    ptr->pa_loop = NULL;
    ptr->stream  = NULL;

    return ptr;
}


void buffpla_init_buff(BufferPlayer* player, size_t sample_cnt){
    if (player->buff_own && sample_cnt < player->buff_size){
        // sufficient memory is allocated -> do nothing
    }else{
        if(player->buff_own && (player->buff)){ // free if owning the pointer
            free(player->buff);
        }
        player->buff = malloc(sample_cnt * sizeof(int16_t));
        player->buff_own = 1;
    }
    player->buff_size = sample_cnt;
    player->buff_off = 0;
}


void buffpla_set_buff(BufferPlayer* player, int16_t* samples, size_t size){
    if(player->buff_own){ // free if owning the pointer
        free(player->buff);
    }

    player->buff = samples;

    player->buff_own = 0;
    player->buff_size = size;
    player->buff_off = 0;

}

void buffpla_free(BufferPlayer* player){
//    for(int i = 0; i < player->buff_size; i++) {
//        fprintf(stdout, "%d\n", player->buff[i]);
//    }
    if(player->buff_own){ // free if owning the pointer
        free(player->buff);
    }
    free(player->pa_af);
    player->pa_af = NULL;

    if (player->stream){
        pa_stream_disconnect(player->stream);
        pa_stream_unref(player->stream);
        player->stream = NULL;
    }

    if(player->pa_ctx){
        pa_operation *o = pa_context_drain(player->pa_ctx, on_drain_ctx_complete, NULL);
        if (o) pa_operation_unref(o);
        player->pa_ctx = NULL;
    }
    if(player->pa_api){
        player->pa_api->quit(player->pa_api, 0);
        player->pa_api = NULL;
    }
    if (player->pa_ctx) {
        pa_context_unref(player->pa_ctx);
        player->pa_ctx = NULL;
    }
    if (player->pa_loop) {
        pa_mainloop_free(player->pa_loop);
    }
    free(player);
}


int buffpla_init(BufferPlayer* player){
    // common variables
    int err_no = 0;
    int retval;
    int ctx_init_status = 0;

    // init resources
    player->pa_loop = pa_mainloop_new();
    player->pa_api  = pa_mainloop_get_api(player->pa_loop);
    player->pa_ctx  = pa_context_new(player->pa_api, "BufferPlayer");

    pa_context_set_state_callback(player->pa_ctx, buffpla_state_listener, &ctx_init_status);

    err_no = pa_context_connect(
            player->pa_ctx,
            NULL, // NULL: default server
            0,     //
            NULL   // callback when new daemon is spawned
    );
    if (err_no != 0){
        fprintf(stderr, "Could not create PulseAudio context. Error %s [%d]\n", pa_strerror(err_no), err_no );
        return 1;
    }

    // Iterate mainloop till PA gets ready
    // 'pa_ready' is set in callback fun 'pa_state_callback'
    int n_sources = 0;
    while (ctx_init_status == 0) {
        n_sources = pa_mainloop_iterate(player->pa_loop, 1, &retval);
        if(n_sources < 1){
            ctx_init_status = 2;
            break;
        }
    }
    if(ctx_init_status != 1){
        fprintf(stdout, "pa_mainloop_iterate could not create any playback resource: err=%d\n", n_sources);
        return 2;
    }

    player->stream = pa_stream_new(player->pa_ctx, "Playback", player->pa_af, NULL);
    if (!player->stream) {
        fprintf(stdout, "[PA ERR]  Failed to create stream");
        return 3;
    }

    pa_stream_set_write_callback(player->stream, buffpla_writer_cb, player);

    err_no = pa_stream_connect_playback(player->stream, NULL, NULL, //&bufattr,
                                        PA_STREAM_INTERPOLATE_TIMING
                                        | PA_STREAM_ADJUST_LATENCY
                                        | PA_STREAM_AUTO_TIMING_UPDATE, NULL, NULL);
    if (err_no != 0) {
        fprintf(stderr, "Could not connect playback stream. Error %s [%d]\n", pa_strerror(err_no), err_no);
        return 4;
    }
    return 0;
}

void null_cb(void* userdata){};

void context_drain_complete(pa_context* ctx, void*) {
    pa_context_disconnect(ctx);
}


void on_drain_ctx_complete(pa_context* ctx, void* _){
//    pa_context_disconnect(ctx);
}

void on_drain_stream_complete(pa_stream* stream, int success, void *userdata){
    fprintf(stdout, "on_drain_stream_complete: Success %d\n", success);
    if (!success) return;
    if(userdata) {
        BufferPlayer* player = (BufferPlayer*) userdata;
        if(!player->stream) return;
//        pa_stream_disconnect(player->stream);
//        pa_stream_unref(player->stream);
//        player->stream = NULL;

//        if(!pa_context_drain(player->pa_ctx, on_drain_ctx_complete, NULL)){
//            pa_context_disconnect(player->pa_ctx);
//        }else{
//            printf("Draining\n");
//        }

        pa_operation *o = pa_context_drain(player->pa_ctx, on_drain_ctx_complete, NULL);
        if(o){
            pa_operation_unref(o);
        }else{
//            pa_context_disconnect(player->pa_ctx);
        }
    }
}

void buffpla_writer_cb(pa_stream *stream, size_t req_len, void *userdata){
    fprintf(stdout, "stream_request_cb %d\n", (int)req_len);

    BufferPlayer* player = (BufferPlayer*) userdata;
    int nbyte_to_write, err_no = 0;
    int rem_len = req_len; // remaining length
    while(rem_len > 0){
        nbyte_to_write = (int)fmin(req_len, (player->buff_off_end - player->buff_off)*2);
        err_no = pa_stream_write(stream, &(player->buff[player->buff_off]),
                nbyte_to_write,null_cb, 0LL, PA_SEEK_RELATIVE);
        if(err_no != 0){
            fprintf(stderr, "failed to write audio stream. Error %s [%d]\n", pa_strerror(err_no), err_no );
            return;
        }
        player->buff_off += nbyte_to_write/2;
        rem_len -= nbyte_to_write;

        if(player->buff_off == player->buff_off_end){
            if(player->is_loop){
                player->buff_off = player->buff_off_start;
            }else{
                pa_operation* o = pa_stream_drain(stream, on_drain_stream_complete, player);
                if (!o){
                    fprintf(stderr, "pa_stream_drain(): %s\n", pa_strerror(pa_context_errno(player->pa_ctx)));
                }else{

                }
                pa_operation_unref(o);
//                pa_operation_unref(pa_stream_drain(stream, on_drain_stream_complete, player));
                pa_mainloop_quit(player->pa_loop, 0);
//                pa_mainloop_free(player->pa_loop);
//                player->pa_api->io_free()
                break;
            }
        }
    }
}


buffpla_state_t buffpla_blocking_play(BufferPlayer* player, int off_start, int off_end, int loop){
    int retval;
    pa_context_state_t state = pa_context_get_state(player->pa_ctx);
    if(PA_CONTEXT_IS_GOOD(state) == 1){
        off_end = off_end <= 0 ? player->buff_size : off_end;
        // range check
        if (off_start < 0) return ERR_OUT_OF_BOUND_OFFSET;
        if (off_end > player->buff_size) return ERR_OUT_OF_BOUND_OFFSET;
        if (off_start == off_end) return ERR_INVALID_OFFSET_RANGE;
        if (off_start > off_end) return ERR_INVALID_OFFSET_RANGE;
        player->buff_off_start = player->buff_off = off_start;
        player->buff_off_end = off_end;
        // start mainloop
        int err_no = pa_mainloop_run(player->pa_loop, &retval);
        if (err_no < 0) {
            fprintf(stderr, "Failing main loop:  '%s' [%d]\n", pa_strerror(err_no), err_no);
        }
        int poll_ret = pa_mainloop_poll(player->pa_loop);
    }else{
        fprintf(stderr, "Cannot play in state:  '%d'\n", state);
        return ERR_AUDIO_NOT_INITIALIZED;
    }
}

