#include <stdio.h>
#include <string.h>
#include <memory.h>
#include <malloc.h>
#include <stdlib.h>
#include "async-play-api.h"

/*
 *
 * Usage
 *
 * $ play
 * $ stop
 * $ pause
 *
 * $ play 4.5
 * $ play 4.5  8
 */

const char * SAMPLE_AUDIO_FILE = TEST_SAMPLE_WAV;  // as defined in 'CMakeLists.txt'

typedef struct Command {
  char* cmd;
  int narg;
  float* arg1;
  float* arg2;
  void (*fun)(BufferPlayer*, float*, float*);
// fun pointer
} Command ;

static int CMD_CNT = 4;

void cb_play(BufferPlayer *player, float* arg1, float* arg2){
    if(arg2){ // with start and stop
        buffpla_blocking_play(player, (int)(*arg1*8000), (int)(*arg2*8000), 0);
    }else if(arg1){
        buffpla_blocking_play(player, (int)(*arg1*8000), -1, 0);
    }else{
        buffpla_blocking_play(player, 0, -1, 0);
    }
}




Command* get_commands(){
    Command* commands = malloc(CMD_CNT * sizeof(Command));

    commands[0].cmd = "play";  commands[0].fun = &cb_play;
    commands[1].cmd = "stop";
    commands[2].cmd = "pause";
    commands[3].cmd = "vol";

    for(int i = 0; i < CMD_CNT ; i++) commands[i].narg = 0;
    return commands;
}

Command* find_cmd(const char* cmd, Command* cmds){
    for(int i = 0; i < CMD_CNT; i++){
        if(strcmp(cmds[i].cmd, cmd) == 0){
            return &cmds[i];
        }
    }
    return NULL;
}


int16_t* read_audio(size_t nsample){
    FILE* fh = fopen(SAMPLE_AUDIO_FILE, "rb");
    fseek(fh, 44, SEEK_SET); // skip 44 byte WAVE header

    int16_t* data = malloc(nsample * sizeof(int16_t));

    int off = 0;
    while(off*2 < nsample){
        off += fread(&(data[off]), 1, 2 * (nsample*2 - off), fh);
    }
    fclose(fh);
    return data;
}



int main() {
    char cmd_buff[100];
    char * word, ts;

    // init player
    int nsample = 8000 * 5; // 5 sec
    int16_t* data = read_audio(nsample);

    BufferPlayer * player = buffpla_new();
    player->is_loop = 0;
    buffpla_set_buff(player, data, nsample);

    int player_status = 0;
    if(player_status == 0) {
        Command* commands = get_commands();
        buffpla_init(player);
        fprintf(stdout, "buffpla> ");
        while(fgets(cmd_buff, 100, stdin)) {
            cmd_buff[strlen(cmd_buff) - 1] = '\0';

            word = strtok(cmd_buff, " ");
//        fprintf(stdout, "'%s' %d\n", word, (int)strlen(cmd_buff));

            if (!word) break;

            if (strcmp(word, "q") == 0 || strcmp(word, "quit") == 0 || strcmp(word, "exit") == 0) {
                break;
            }
            Command *cmd = find_cmd(word, commands);

            if (!cmd) {
                fprintf(stderr, "Cannot parse input '%s'", cmd_buff);
                continue;
            }

            word = strtok(NULL, " ");
            if (word) {
                cmd->narg++;
                cmd->arg1 = malloc(sizeof(float));
                *(cmd->arg1) = atof(word);
                word = strtok(NULL, " ");
                if (word) {
                    cmd->arg2 = malloc(sizeof(float));
                    *(cmd->arg2) = atof(word);
                }
            }
            fprintf(stdout, "%s\n", cmd->cmd);

            // call registered function
            (*cmd->fun)(player, cmd->arg1, cmd->arg2);

            // free arguments
            if(cmd->arg1) free(cmd->arg1);
            if(cmd->arg2) free(cmd->arg2);

            fprintf(stdout, "buffpla> ");
        }
    }else{
        fprintf(stderr, "Cannot start Buffer Player: ERR_NO=%d\n", player_status);
    }
    fprintf(stdout, "Done.");
    buffpla_free(player);
    free(data);
}
