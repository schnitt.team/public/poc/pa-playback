#ifndef PA_PLAYBACK_ASYNC_PLAY_API_H
#define PA_PLAYBACK_ASYNC_PLAY_API_H
PA_PLAYBACK_SRC_THREADED_PLAY_API_INCLUDE_THREADED_PLAY_API_H_
#include  <stddef.h>
#include <stdint.h>
#include <pulse/pulseaudio.h>
#include <pulse/def.h>


typedef enum BUFFPLA_STATUS {
  OK      = 0,
  ERR_AUDIO_NOT_INITIALIZED  = 111,
  ERR_OUT_OF_BOUND_OFFSET    = 410,
  ERR_INVALID_OFFSET_RANGE   = 411,
} buffpla_state_t;


/**
 *
 */
typedef struct BufferPlayer{
    int16_t* buff;  // buffer of samples
    int buff_size;  // buffer size in samples
    int buff_off;   // offset within the buffer in samples
    int buff_off_start;   // offset start
    int buff_off_end;     // offset end
    int buff_own;   // flag if owning the buff array -> to be freed or not
    int is_loop;  // flog to loop audio or not
    //
    pa_stream*        stream;
    pa_mainloop*     pa_loop;
    pa_mainloop_api*  pa_api;
    pa_context*       pa_ctx;
    pa_sample_spec*    pa_af;
} BufferPlayer;


/**
 * Creates an audio player for int16 buffers.
 * @return buffer audio player.
 */
BufferPlayer* buffpla_new();


/**
 * Allocate data for 'len' number of samples
 * @param player
 * @param len
 */
void buffpla_init_buff(BufferPlayer* player, size_t sample_cnt);


/**
 *
 * @param player player context
 * @param samples  data array
 * @param size  size in samples
 */
void buffpla_set_buff(BufferPlayer* player, int16_t* samples, size_t size);

/**
 *
 * @param player
 * @param off_start start offset in samples.
 * @param off_end end offset in samples. Setting it to -1 converts to full buffer length.
 * @param loop  if the audio should loop.
 */
buffpla_state_t buffpla_blocking_play(BufferPlayer* player, int off_start, int off_end, int loop);

void buffpla_nonblocking_play(BufferPlayer* player, int off_start, int off_end, int loop);


void buffpla_free(BufferPlayer* player);


///////////////////////////////////////////////////////////////
///               PULSE AUDIO related functions             ///
///////////////////////////////////////////////////////////////

/**
 * This callback gets called when the context changes state.
 * @param ctx
 * @param userdata
 */
void buffpla_state_listener(pa_context *ctx, void *userdata);


/**
 *
 * @param player
 * @return
 */
int buffpla_init(BufferPlayer* player);

void buffpla_writer_cb(pa_stream *stream, size_t req_len, void *userdata);

void on_drain_stream_complete(pa_stream* stream, int success, void *userdata);

void on_drain_ctx_complete(pa_context* ctx, void* userdata);


#endif //PA_PLAYBACK_ASYNC_PLAY_API_H
