

#add_executable(async-play-api
#        include/async-play-api.h
#        src/async-play-api.c
#        src/main.c
#)

add_library(async-play-api STATIC
        include/async-play-api.h
        src/async-play-api.c
)


target_include_directories(async-play-api
    PUBLIC include
)

target_link_libraries(async-play-api
        PUBLIC PulseAudio::PulseAudio
        m
)

add_executable(async-play-api-test
        src/main.c
)

target_link_libraries(async-play-api-test
        PRIVATE async-play-api
)

target_compile_definitions(async-play-api-test PRIVATE
        TEST_SAMPLE_WAV="${CMAKE_CURRENT_SOURCE_DIR}/../../data/OSR_us_000_0019_5s_8k.wav")


add_executable(async-play-api-cli
        src/cli.c
)

target_link_libraries(async-play-api-cli
        PRIVATE async-play-api
)

target_compile_definitions(async-play-api-cli PRIVATE
        TEST_SAMPLE_WAV="${CMAKE_CURRENT_SOURCE_DIR}/../../data/OSR_us_000_0019_5s_8k.wav")

