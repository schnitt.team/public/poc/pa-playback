# Dev notes

## Blocking/Simple Playback

| Function         | Brief                              |
|------------------|------------------------------------|
| `pa_simple_new`  | creates audio client               |
| `pa_simple_write` | plays audio                        |
| `pa_simple_drain` | plays last samples in audio buffer |
| `pa_simple_free` | disposes client                    |


