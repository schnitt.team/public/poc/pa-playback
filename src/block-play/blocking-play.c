#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>

#include <pulse/simple.h>
#include <pulse/error.h>

/*
 * Simple - not-asynchronous - audio playback
 * for demonstrative purposes.
 * Based on https://freedesktop.org/software/pulseaudio/doxygen/pacat-simple_8c-example.html
 */


const char * SIMPLE_PLAY_WAV     = SAMPLE_WAV;  // as defined in 'CMakeLists.txt'
const int SIMPLE_PLAY_HZ         =       8000;
const int SIMPLE_PLAY_CH         =          1;
const int SIMPLE_PLAY_BUFF_SIZE  =      16000;  // 1 sec buffer

int main() {

  static const pa_sample_spec ss = {
      .format   = PA_SAMPLE_S16LE,
      .rate     =  SIMPLE_PLAY_HZ,
      .channels =  SIMPLE_PLAY_CH
  };

  pa_simple *s = NULL;
  int ret = 1;
  int error;

  /* Create a new playback stream */
  if (!(s = pa_simple_new(
      NULL,             // default server
      "PulseAudio demo",           //
      PA_STREAM_PLAYBACK,  // direction: playback (not recording)
      NULL,               // default sink
      "playback",  // stream name
      &ss,                    // sample type
      NULL,             // default channel map
      NULL,             // default buffering attributes
      &error
      ))) {
    fprintf(stderr, __FILE__": pa_simple_new() failed: %s\n", pa_strerror(error));
    goto finish;
  }

  FILE* fh = fopen(SIMPLE_PLAY_WAV, "rb");
  fseek(fh, 44, SEEK_SET); // skip 44 byte WAVE header

  int tot_off = 0;
  for (;;) {
    int16_t buf[SIMPLE_PLAY_BUFF_SIZE];  // 16bit audio sample depth
    ssize_t off;

    {
      pa_usec_t latency;
      if ((latency = pa_simple_get_latency(s, &error)) == (pa_usec_t) -1) {
        fprintf(stderr, __FILE__": pa_simple_get_latency() failed: %s\n", pa_strerror(error));
        goto finish;
      }
      fprintf(stderr, "%0.0f usec    \r", (float)latency);
      fflush(stderr);
    }

    // read in data
    off = fread(&buf, 1, sizeof(buf), fh);
    tot_off += off/2;

    fprintf(stdout, "playback offset=%d\n", tot_off);
    if(off == 0) break;  /* EOF */
    if(off <= 0){  /*  read less than 0 -> error */
      fprintf(stderr, __FILE__": read() failed: %s\n", strerror(errno));
      goto finish;
    };

    // play audio - by writing buffer
    // and BLOCK till it is written
    if (pa_simple_write(s, buf, (size_t) off, &error) < 0) {
      fprintf(stderr, __FILE__": pa_simple_write() failed: %s\n", pa_strerror(error));
      goto finish;
    }
  }

  // drain remaining samples
  if (pa_simple_drain(s, &error) < 0) {
    fprintf(stderr, __FILE__": pa_simple_drain() failed: %s\n", pa_strerror(error));
    goto finish;
  }

  ret = 0; // no error if got this far

finish:
  if (s)
    pa_simple_free(s);

  return ret;
}
