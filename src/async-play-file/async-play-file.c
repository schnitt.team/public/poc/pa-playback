#include <stdio.h>
#include <string.h>
#include <pulse/pulseaudio.h>
#include <pulse/def.h>
#include <math.h>

// cf. https://www.freedesktop.org/wiki/Software/PulseAudio/Documentation/Developer/Clients/Samples/AsyncPlayback/
// cf. http://www.bic.mni.mcgill.ca/software/MDP/HTML/VOLUME_IO_prog_ref.html/volume-source-code-example.html

const char * SAMPLE_AUDIO_FILE = SAMPLE_WAV;  // as defined in 'CMakeLists.txt'


typedef struct pa_loop_ctx {
  pa_mainloop*         pa_loop;
  pa_mainloop_api*     pa_api;
  pa_context*          pa_ctx;
} pa_ctx_t;


/**
 * Generates audio buff form of specified duration.
 * @param dur_in_sec
 * @param hz
 * @return non owning pointer
 */
typedef struct wave_data {
  FILE* fh;
  int16_t* buff;  // buffer of samples
  int buff_size;   // buff_size in samples
  int buff_off;    // offset in samples
  int tot_off;   // in samples
  int loaded; // available in samples
  pa_ctx_t* ctx;
} wave_data_t;




/**
 * Disconnect from PulseAudio server and free allocated resources.
 * @param loop
 */
void pa_loop_ctx_free(pa_ctx_t* loop) {
  pa_context_disconnect(loop->pa_ctx);
  pa_context_unref(loop->pa_ctx);
  pa_mainloop_free(loop->pa_loop);
}

/**
 * This callback gets called when the context changes state.
 * We really only care about when it's ready or if it has failed
 * @param c
 * @param userdata
 */
void pa_state_callback(pa_context *ctx, void *userdata) {
  int* pa_ready = (int*)userdata;
  pa_context_state_t state = pa_context_get_state(ctx);
  switch  (state) {
    // These are just here for reference
    case PA_CONTEXT_UNCONNECTED:
      fprintf(stderr, "[PA_STATUS]  unconnected\n");
      break;
    case PA_CONTEXT_CONNECTING:
      fprintf(stderr, "[PA_STATUS]  connecting\n");
      break;
    case PA_CONTEXT_AUTHORIZING:
      fprintf(stderr, "[PA_STATUS]  authorizing\n");
      break;
    case PA_CONTEXT_SETTING_NAME:
      fprintf(stderr, "[PA_STATUS]  setting context name\n");
      break;
    case PA_CONTEXT_FAILED:
      fprintf(stderr, "[PA_STATUS]  context failed\n");
      break;
    case PA_CONTEXT_TERMINATED:
      fprintf(stderr, "[PA_STATUS]  context terminated\n");
      *pa_ready = 2;
      break;
    case PA_CONTEXT_READY:
      fprintf(stderr, "[PA_STATUS]  context ready\n");
      *pa_ready = 1;
      break;
    default:
      fprintf(stderr, "[PA_STATUS]  uknown state: %d\n", state);
      break;
  }
}


/**
 * Allocates and initializes PulseAudio objects.
 * @return a struct with initialized objects.
 */
pa_ctx_t* pa_loop_ctx_new(){
  pa_ctx_t* loop_ctx = (pa_ctx_t*) malloc(sizeof(pa_ctx_t));
  // Create PulseAudio mainloop API and connection to the default server
  loop_ctx->pa_loop = pa_mainloop_new();
  loop_ctx->pa_api  = pa_mainloop_get_api(loop_ctx->pa_loop);
  loop_ctx->pa_ctx  = pa_context_new(loop_ctx->pa_api, "Dev: async playback file");

  return loop_ctx;
}

wave_data_t* gen_audio_data(const int buff_in_sec, const double hz){
  wave_data_t* data = malloc(sizeof(wave_data_t));

  data->fh = fopen(SAMPLE_AUDIO_FILE, "rb");
  fseek(data->fh, 44, SEEK_SET); // skip 44 byte WAVE header

  data->buff_size = (int)(buff_in_sec * hz);  // number of samples
  data->buff = malloc(data->buff_size * sizeof(int16_t));
  data->buff_off = 0;
  data->tot_off = 0;
  data->loaded = 0;
  data->ctx = pa_loop_ctx_new();
  return data;
}

/**
 * Just a placeholder to avoid copying sample in 'pa_stream_write'
 */
void null_fun(void* userdata){};


int read_data(void *userdata, int req_in_byte){
  wave_data_t * data = (wave_data_t*) userdata;
  // filled: reset
  if(data->loaded == data->buff_size){ // full cannot read
    if(data->buff_off == data->loaded){
      data->buff_off = 0;
      data->loaded = 0;
    }else{
      return 0;
    }
  }

  int nbytes;
  if(data->fh){
    if(data->loaded < data->buff_size) {
      if (data->loaded + req_in_byte >= data->buff_size) { // cannot read all
        // read till end of this buffer
        nbytes = fread(&(data->buff[data->loaded]), 1, 2 * (data->buff_size - data->loaded), data->fh);
      } else {
        nbytes = fread(&(data->buff[data->loaded]), 1, req_in_byte, data->fh);
      }
      fprintf(stdout, "Loaded %d bytes\n", nbytes);
      if(nbytes <= 0) {
        fprintf(stdout, "Closing audio file\n");
        fclose(data->fh);
        data->fh = NULL;
      }else{
        data->loaded += nbytes / 2;
      }
    }else{
      fprintf(stderr, "What?");
    }
    return nbytes;
  }else{
    return 0;
  }

}

void context_drain_complete(pa_context* ctx, void*) {
  pa_context_disconnect(ctx);
}

/* Stream draining complete */
void stream_drain_complete(pa_stream* stream, int success, void *userdata) {
  fprintf(stdout, "stream_drain_complete\n");
  wave_data_t* data = NULL;
  if(userdata){
    data = (wave_data_t*) userdata;
    if(data->fh){
      fclose(data->fh);
    }
    pa_stream_disconnect(stream);
    pa_stream_unref(stream);
    stream = NULL;

    pa_operation *o = pa_context_drain(data->ctx->pa_ctx, context_drain_complete, NULL);
    pa_context_disconnect(data->ctx->pa_ctx);
    if (o) {
      pa_operation_unref(o);
    }
  }

  if (success) {
    fprintf(stderr, "Drained stream\n");
  }else{
    fprintf(stderr, "Failed to drain stream\n");
  }
  if(data){
    data->ctx->pa_api->quit(data->ctx->pa_api, 0);
  }
}


void stream_request_cb(pa_stream *stream, size_t req_len, void *userdata) {
  fprintf(stdout, "stream_request_cb %d\n", (int)req_len);
  wave_data_t* data = (wave_data_t*) userdata;
  int nbyte_avail, nbyte_to_write;

  size_t length = req_len;
  while(length > 0){
    nbyte_avail = (data->loaded - data->buff_off) * 2;
    if(nbyte_avail < 2 * req_len){
      read_data(data, 2 * length);
      nbyte_avail = (data->loaded - data->buff_off) * 2;
      if (nbyte_avail == 0){ // EOF
//        fprintf(stdout, "End-of-File\n");
        pa_operation_unref(pa_stream_drain(stream, stream_drain_complete, data));
        return;
      }
    }
    nbyte_to_write = (int)fmin(length, nbyte_avail);
//    if(nbyte_to_write == 0)
    fprintf(stdout, "%6d buff_off %6d buff_size %6d tot_off %6d req; %6d avail, %6d to-write\n", 2*data->buff_off, 2*data->buff_size, 2*data->tot_off, (int)length, nbyte_avail, nbyte_to_write);
    int err_no = pa_stream_write(
        stream, &(data->buff[data->buff_off]),
        nbyte_to_write,
        null_fun, 0LL, PA_SEEK_RELATIVE);
    if(err_no != 0){
      fprintf(stderr, "failed to write audio stream. Error %s [%d]\n", pa_strerror(err_no), err_no );
      return;
    }
    data->buff_off += nbyte_to_write / 2;
    data->tot_off +=  nbyte_to_write / 2;
    length -= nbyte_to_write;
    fprintf(stdout, "%6d buff_off %6d buff_size %6d tot_off\n", 2*data->buff_off, 2*data->buff_size, 2*data->tot_off);
  }
}

void success(pa_stream*s, int success, void *userdata){
  fprintf(stdout, "success");
};



int main() {
  int err_no;
  int retval;
  int pa_ready = 0;
  pa_sample_spec af = {PA_SAMPLE_S16LE, 8000, 1};

  wave_data_t* data = gen_audio_data(2, af.rate);

  pa_context_set_state_callback(data->ctx->pa_ctx, pa_state_callback, &pa_ready);

  err_no = pa_context_connect(
      data->ctx->pa_ctx,
      NULL, // NULL: default server
      0,     //
      NULL   // callback when new daemon is spawned
      );
  if (err_no != 0){
    fprintf(stderr, "Could not create PulseAudio context. Error %s [%d]\n", pa_strerror(err_no), err_no );
    pa_loop_ctx_free(data->ctx);
    return 1;
  }

  // Iterate mainloop till PA gets ready
  // 'pa_ready' is set in callback fun 'pa_state_callback'
  while (pa_ready == 0) {
    int n_sources = pa_mainloop_iterate(data->ctx->pa_loop, 1, &retval);
    if(n_sources < 1){
      fprintf(stdout, "pa_mainloop_iterate could not create any sources: err=%d\n", n_sources);
      return 3;
    }
  }
  if (pa_ready == 2) {
    return -1;
  }

  pa_stream *playstream = pa_stream_new(data->ctx->pa_ctx, "Playback", &af, NULL);
  if (!playstream) {
    printf("pa_stream_new failed\n");
  }

  pa_stream_set_write_callback(playstream, stream_request_cb, data);
//  pa_stream_set_underflow_callback(playstream, stream_underflow_cb, data);
//  pa_stream_set_started_callback(playstream, stream_underflow_cb, data);
//  pa_stream_set_latency_update_callback(playstream, stream_underflow_cb, data);

  pa_buffer_attr bufattr;
  bufattr.fragsize = (uint32_t)-1;
  bufattr.maxlength =  (uint32_t) -1; // pa_usec_to_bytes(latency,&af);
  bufattr.minreq    = pa_usec_to_bytes(1000 * 1000,&af);// (uint32_t) -1; //
  bufattr.tlength   = pa_usec_to_bytes( 100 * 1000, &af); // 1 sec buffer
  bufattr.prebuf    =    (uint32_t)-1;

  err_no = pa_stream_connect_playback(playstream, NULL, NULL, //&bufattr,
                                 PA_STREAM_INTERPOLATE_TIMING
                                     | PA_STREAM_ADJUST_LATENCY
                                     | PA_STREAM_AUTO_TIMING_UPDATE, NULL, NULL);
//  err_no = pa_stream_connect_playback(playstream, NULL, NULL,PA_STREAM_INTERPOLATE_TIMING, NULL, NULL);
  if (err_no != 0) {
    fprintf(stderr, "Could not connect playback stream. Error %s [%d]\n", pa_strerror(err_no), err_no);
    return 2;
  }

  { // status print
    pa_context_state_t state = pa_context_get_state(data->ctx->pa_ctx);
    retval = PA_CONTEXT_IS_GOOD(state);
    if(retval != 1){
      fprintf(stdout, "Error state before audio loop: %d  OK=%d\n", state, retval);
      return 3;
    }
    fprintf(stdout, "State before audio loop: %d  OK=%d\n", state, retval);
  }

//  pa_stream_trigger(playstream, success, data);
  // Run the mainloop until pa_mainloop_quit() is called
  // (this example never calls it, so the mainloop runs forever).
  err_no = pa_mainloop_run(data->ctx->pa_loop, &retval);
  if (err_no < 0) {
    fprintf(stderr, "Failing main loop:  '%s' [%d]\n", pa_strerror(err_no), err_no);
  }

//  if(playstream)
//    pa_stream_unref(playstream);
  if (data->ctx->pa_ctx)
    pa_context_unref(data->ctx->pa_ctx);
  if (data->ctx->pa_loop)
    pa_mainloop_free(data->ctx->pa_loop);

  if(data->buff){
    free(data->buff);
    data->buff = NULL;
  }
  free(data->ctx);
  free(data);
  return 0;
}
